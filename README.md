

## Testing
PyTest follows [standard test discovery rules](https://docs.pytest.org/en/7.3.x/explanation/goodpractices.html#conventions-for-python-test-discovery).

## URIs naming convention
[Source](https://restfulapi.net/resource-naming/)

- Use hyphens (-) to improve the readability of URIs
  - http://api.example.com/device-management/managed-devices
- Do not use trailing slash (/) in URIs
- Use lowercase letters in URIs
- Never use CRUD function names in URIs
