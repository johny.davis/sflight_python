import numbers
from typing import List, Optional

import falcon
import peewee
import pydantic
from spectree import Response, Tag

from settings import api

# ============ Globals ============

database = peewee.SqliteDatabase(None)

airport_tag = Tag(name="Airports")
city_tag = Tag(name="Cities")
country_tag = Tag(name="Countries")
flight_tag = Tag(name="Flights")
booking_tag = Tag(name="Bookings")


# ============ ORM Fields ============

class SAPDateField(peewee.DateField):
    formats = [
        '%Y-%m-%d',
        '%Y-%m-%d %H:%M:%S',
        '%Y-%m-%d %H:%M:%S.%f',
        "%Y%m%d"
    ]
    format = "%Y%m%d"

    def adapt(self, value):
        value = super().adapt(value)
        if isinstance(value, numbers.Number):
            pp = lambda x: x.date()
            peewee.format_date_time(str(value), self.formats, pp)
        return value

    def python_value(self, value):
        value = super().python_value(value)
        try:
            return value.strftime(self.format)
        except AttributeError:
            return value


class SAPTimeField(peewee.TimeField):
    formats = [
        '%H:%M:%S.%f',
        '%H:%M:%S',
        '%H:%M',
        '%Y-%m-%d %H:%M:%S.%f',
        '%Y-%m-%d %H:%M:%S',
        "%H%M%S"
    ]
    format = "%H%M%S"

    def adapt(self, value):
        value = super().adapt(value)
        if isinstance(value, numbers.Number):
            pp = lambda x: x.time()
            peewee.format_date_time(str(value), self.formats, pp)
        return value

    def python_value(self, value):
        value = super().python_value(value)
        try:
            return value.strftime(self.format)
        except AttributeError:
            return value
    
    
class SAPDecimalField(peewee.DecimalField):
    def python_value(self, value):
        if value is not None:
            return float(value)


class SAPBoolField(peewee.BooleanField):
    true: str = "X"
    false: str = " "

    def db_value(self, value):
        if isinstance(value, bool):
            return value
        elif isinstance(value, str) and value.upper() in [self.false, self.true]:
            return True if value == "X" else False
        else:
            raise ValueError('value for boolean field must be bool '
                             f'or a str: `{self.false}` (whitespace) for False or `{self.true}` for True.')

    def python_value(self, value: bool):
        return self.true if value else self.false


class NumericField(peewee.CharField):
    def db_value(self, value):
        try:
            int(value)
        except ValueError as e:
            raise ValueError("Value must be numeric")
        return value


# ============ Models ============


class BaseModel(peewee.Model):
    class Meta:
        database = database


class TimezoneRuleModel(BaseModel):
    """Representation of TTZR table in SAP"""
    zonerule = peewee.CharField(max_length=6, primary_key=True, column_name="zonerule")
    utcdiff = SAPTimeField()
    utcsign = peewee.CharField(max_length=1)
    flagactive = SAPBoolField()


class SummerTimeRuleModel(BaseModel):
    """Representation of TTZR table in SAP"""
    dstrule = peewee.CharField(max_length=6, primary_key=True, column_name="dstrule")
    dstdiff = SAPTimeField()
    flagactive = SAPBoolField()


class TimezoneModel(BaseModel):
    """Representation of TTZZ table in SAP"""
    tzone = peewee.CharField(max_length=6, primary_key=True)
    zonerule = peewee.ForeignKeyField(TimezoneRuleModel, "zonerule", column_name="zonerule")  # foreign key
    dstrule = peewee.ForeignKeyField(SummerTimeRuleModel, "dstrule", column_name="dstrule")  # foreign key
    flagactive = SAPBoolField()


class CurrencyModel(BaseModel):
    """Representation of SCURX table in SAP"""
    currkey = peewee.CharField(max_length=5, primary_key=True)
    currdec = peewee.SmallIntegerField()


class CityModel(BaseModel):
    """Representation of SGEOCITY table in SAP"""
    city = peewee.CharField(max_length=20)
    country = peewee.CharField(max_length=3)
    latitude = SAPDecimalField(max_digits=16, decimal_places=16)
    longitude = SAPDecimalField(max_digits=16, decimal_places=16)

    class Meta:
        """Metaclasses are deeper magic than 99% of users should ever worry about.
        If you wonder whether you need them, you don't"""
        primary_key = peewee.CompositeKey("city", "country")


class AirlineModel(BaseModel):
    """Representation of SCARR table in SAP"""
    carrid = peewee.CharField(max_length=3, primary_key=True, column_name="carrid")  # Airline code
    carrname = peewee.CharField(max_length=20)  # Airline name
    currcode = peewee.ForeignKeyField(CurrencyModel, "currkey",
                                      column_name="currcode")  # foreign key - Local currency of airline
    url = peewee.CharField(max_length=255)  # Airline URL


class AirportModel(BaseModel):
    """Representation of SAIRPORT table in SAP"""
    id = peewee.CharField(max_length=3, primary_key=True)  # Airport code
    name = peewee.CharField(max_length=25)  # Airport name
    time_zone = peewee.ForeignKeyField(TimezoneModel, "tzone",
                                       column_name="time_zone")  # foreign key - Airport time zone


class PlaneModel(BaseModel):
    """Representation of SAPLANE table in SAP"""
    planetype = peewee.CharField(max_length=10, primary_key=True, help_text="aircraft type")
    seatsmax = peewee.IntegerField(help_text="maximum capacity in economy class")
    consum = SAPDecimalField(max_digits=16, decimal_places=16, help_text="average consumption")
    con_unit = peewee.CharField(max_length=3, help_text="unit of fuel consumption")
    tankcap = SAPDecimalField(max_digits=16, decimal_places=4, help_text="maximum fuel capacity")
    cap_unit = peewee.CharField(max_length=3, help_text="mass unit")
    weight = SAPDecimalField(max_digits=16, decimal_places=4, help_text="maximum fuel capacity")
    wei_unit = peewee.CharField(max_length=3, help_text="mass unit of weight")
    span = SAPDecimalField(max_digits=16, decimal_places=16, help_text="span")
    span_unit = peewee.CharField(max_length=3, help_text="span unit")
    leng = SAPDecimalField(max_digits=16, decimal_places=16, help_text="length of plane")
    leng_unit = peewee.CharField(max_length=3, help_text="length unit")
    op_speed = SAPDecimalField(max_digits=17, decimal_places=4, help_text="speed")
    speed_unit = peewee.CharField(max_length=3, help_text="speed unit")
    producer = peewee.CharField(max_length=5, help_text="plan manufacturer")
    seatsmax_b = peewee.IntegerField(help_text="maximum capacity in business class")
    seatsmax_f = peewee.IntegerField(help_text="maximum capacity in first class")


class FlightScheduleModel(BaseModel):
    """Representation of SPFLI table in SAP"""
    carrid = peewee.ForeignKeyField(AirlineModel, "carrid", column_name="carrid")
    connid = peewee.CharField(max_length=4, column_name="connid")
    countryfr = peewee.CharField(max_length=3)  # foreign key
    cityfrom = peewee.CharField(max_length=20)  # foreign key
    airpfrom = peewee.ForeignKeyField(AirportModel, "id", null=True, column_name="airpfrom")  # foreign key
    countryto = peewee.CharField(max_length=3)  # foreign key
    cityto = peewee.CharField(max_length=20)  # foreign key
    airpto = peewee.ForeignKeyField(AirportModel, "id", null=True, column_name="airpto")  # foreign key
    fltime = peewee.IntegerField()  # TODO: check this in SAP, it's crazy
    deptime = SAPTimeField()
    arrtime = SAPTimeField()
    distance = SAPDecimalField(9, 4)
    distid = peewee.CharField(max_length=3)
    fltype = peewee.CharField(max_length=3)
    period = peewee.CharField(max_length=3)

    class Meta:
        primary_key = peewee.CompositeKey('carrid', 'connid')
        constraints = [
            peewee.SQL('foreign key (cityfrom, countryfr) references citymodel(city, country)'),
            peewee.SQL('foreign key (cityto, countryto) references citymodel(city, country)')
        ]
        depends_on = [CityModel]


class FlightModel(BaseModel):
    """Representation of SFLIGHT table in SAP"""
    carrid = peewee.CharField(max_length=3)  # primary/foreign key
    # connid = NumericField(max_length=4)  # primary/foreign key
    connid = peewee.CharField(max_length=4)  # primary/foreign key
    fldate = SAPDateField()  # primary key
    price = SAPDecimalField(15, 2)  # TODO: create SAP currency field repr
    currency = peewee.ForeignKeyField(CurrencyModel, "currkey", column_name="currency")  # foreign key
    planetype = peewee.ForeignKeyField(PlaneModel, "planetype", column_name="planetype")  # foreign key
    seatsmax = peewee.IntegerField()
    seatsocc = peewee.IntegerField()
    paymentsum = SAPDecimalField(17, 2)  # TODO: create SAP currency field repr
    seatsmax_b = peewee.IntegerField()
    seatsocc_b = peewee.IntegerField()
    seatsmax_f = peewee.IntegerField()
    seatsocc_f = peewee.IntegerField()

    class Meta:
        primary_key = peewee.CompositeKey('carrid', 'connid', 'fldate')
        constraints = [
            peewee.SQL("foreign key (carrid, connid) references flightschedulemodel(carrid, connid)"),
        ]
        depends_on = [FlightScheduleModel]


class LanguageModel(BaseModel):
    """Representation of T002 table in SAP"""
    spras = peewee.CharField(max_length=1, primary_key=True, column_name="spras")  # TODO: add LANG field representation?
    laspez = peewee.CharField(max_length=1, help_text="language specifications")
    lahq = peewee.CharField(max_length=1, null=True, help_text="degree of translation of language")
    laiso = peewee.CharField(max_length=2, null=True, help_text="2-character SAP language code")


class CustomerModel(BaseModel):
    """Representation of SCUSTOM table in SAP"""
    id = NumericField(max_length=8, primary_key=True, column_name="id")  # primary key
    name = peewee.CharField(max_length=25)
    form = peewee.CharField(max_length=15)
    street = peewee.CharField(max_length=30)
    postbox = peewee.CharField(max_length=10)
    postcode = peewee.CharField(max_length=10)
    city = peewee.CharField(max_length=25)
    country = peewee.CharField(max_length=3)
    region = peewee.CharField(max_length=3)
    telephone = peewee.CharField(max_length=30)
    custtype = peewee.CharField(max_length=1)
    discount = peewee.CharField(max_length=3)
    langu = peewee.ForeignKeyField(LanguageModel, "spras", column_name="langu")  # foreign key
    email = peewee.CharField(max_length=40)
    webuser = peewee.CharField(max_length=25)


class UnitModel(BaseModel):
    """Representation of T006 table in SAP"""
    # required fields
    msehi = peewee.CharField(max_length=3, primary_key=True, column_name="msehi")
    # optional fields
    # TODO: add optional fields
    # ...


class CounterModel(BaseModel):
    """Representation of SCOUNTER table in SAP"""
    carrid = peewee.ForeignKeyField(AirlineModel, "carrid", column_name="carrid")  # primary/foreign key
    countnum = NumericField(max_length=8, column_name="countnum")  # primary key
    airport = peewee.ForeignKeyField(AirportModel, "id", null=True, column_name="airport")  # foreign key

    class Meta:
        primary_key = peewee.CompositeKey('carrid', 'countnum')


class AgencyModel(BaseModel):
    """Representation of STRAVELAG table in SAP"""
    agencynum = NumericField(max_length=8, primary_key=True, column_name="agencynum")  # primary key
    name = peewee.CharField(max_length=25)
    street = peewee.CharField(max_length=30)
    postbox = peewee.CharField(max_length=10)
    postcode = peewee.CharField(max_length=10)
    city = peewee.CharField(max_length=25)
    country = peewee.CharField(max_length=3)
    region = peewee.CharField(max_length=3)
    telephone = peewee.CharField(max_length=30)
    url = peewee.CharField(max_length=255)
    langu = peewee.ForeignKeyField(LanguageModel, "spras", column_name="langu")  # foreign key
    currency = peewee.CharField(max_length=5)  # TODO: add CUKY field representation?


class BookingModel(BaseModel):
    """Representation of SBOOK table in SAP"""
    # required fields
    carrid = peewee.CharField(max_length=3)  # primary/foreign key
    connid = peewee.CharField(max_length=4)  # primary/foreign key
    fldate = SAPDateField()  # primary/foreign key
    bookid = NumericField(max_length=8)  # primary key
    customid = peewee.ForeignKeyField(CustomerModel, "id", column_name="customid")  # foreign key
    custtype = peewee.CharField(max_length=1)
    smoker = SAPBoolField()
    luggweight = peewee.SmallIntegerField()
    wunit = peewee.ForeignKeyField(UnitModel, "msehi", column_name="wunit")  # foreign key
    invoice = SAPBoolField()
    class_ = peewee.CharField(max_length=1, column_name="class")  # FIXME: `class` is reserved word
    forcuram = SAPDecimalField(15, 2)
    forcurkey = peewee.ForeignKeyField(CurrencyModel, "currkey", column_name="forcurkey")  # foreign key
    loccuram = SAPDecimalField(15, 2)
    loccurkey = peewee.ForeignKeyField(CurrencyModel, "currkey", column_name="loccurkey")  # foreign key
    order_date = SAPDateField()
    # optional fields
    counter = peewee.ForeignKeyField(CounterModel, "countnum", null=True, column_name="counter")  # foreign key
    agencynum = peewee.ForeignKeyField(AgencyModel, "agencynum", null=True, column_name="agencynum")  # foreign key
    cancelled = SAPBoolField(null=True)
    reserved = SAPBoolField(null=True)
    passname = peewee.CharField(max_length=25, null=True)
    passform = peewee.CharField(max_length=15, null=True)
    passbirth = SAPDateField(null=True)

    class Meta:
        primary_key = peewee.CompositeKey('carrid', 'connid', 'fldate', 'bookid')
        constraints = [
            peewee.SQL("foreign key (carrid, connid, fldate) references flightmodel(carrid, connid, fldate)"),
        ]
        depends_on = [FlightModel]


# ============ Validators ============

class TimezoneRuleValidator(pydantic.BaseModel):
    zonerule: str = pydantic.Field(..., max_length=6, description="Timezone rule")
    utcdiff: str = pydantic.Field(..., description="UTC difference")
    utcsign: str = pydantic.Field(..., max_length=1, description="UTC sign")
    flagactive: bool = pydantic.Field(..., description="Active flag")


class SummerTimeRuleValidator(pydantic.BaseModel):
    dstrule: str = pydantic.Field(..., max_length=6, description="Summer time rule")
    dstdiff: str = pydantic.Field(..., description="DST difference")
    flagactive: bool = pydantic.Field(..., description="Active flag")


class TimezoneValidator(pydantic.BaseModel):
    tzone: str = pydantic.Field(..., max_length=6, description="Timezone")
    zonerule: str = pydantic.Field(..., description="Timezone rule")
    dstrule: str = pydantic.Field(..., description="Summer time rule")
    flagactive: bool = pydantic.Field(..., description="Active flag")


class CurrencyValidator(pydantic.BaseModel):
    currkey: str = pydantic.Field(..., max_length=5, description="Currency key")
    currdec: int = pydantic.Field(..., description="Currency decimal places")


class CityValidator(pydantic.BaseModel):
    city: str = pydantic.Field(..., max_length=20, description="City name")
    country: str = pydantic.Field(..., max_length=3, description="Country code")
    latitude: float = pydantic.Field(..., description="Latitude")
    longitude: float = pydantic.Field(..., description="Longitude")


class CitiesValidator(pydantic.BaseModel):
    cities: List[CityValidator]


class CountryValidator(pydantic.BaseModel):
    country: str = pydantic.Field(..., max_length=3, description="Country code")


class CountriesValidator(pydantic.BaseModel):
    countries: List[CountryValidator]


class AirlineValidator(pydantic.BaseModel):
    carrid: str = pydantic.Field(..., max_length=3, description="Airline code")
    carrname: str = pydantic.Field(..., max_length=20, description="Airline name")
    currcode: str = pydantic.Field(..., description="Local currency of airline")
    url: str = pydantic.Field(..., max_length=255, description="Airline URL")


class AirlinesValidator(pydantic.BaseModel):
    airlines: List[AirlineValidator]


class AirportValidator(pydantic.BaseModel):
    id: str = pydantic.Field(..., max_length=3, description="Airport code")
    name: str = pydantic.Field(..., max_length=25, description="Airport name")
    time_zone: str = pydantic.Field(..., description="Airport time zone")


class AirportsValidator(pydantic.BaseModel):
    airports: List[AirportValidator]


class PlaneValidator(pydantic.BaseModel):
    planetype: str = pydantic.Field(..., max_length=10, description="Aircraft type")
    seatsmax: int = pydantic.Field(..., description="Maximum capacity in economy class")
    consum: float = pydantic.Field(..., description="Average consumption")
    con_unit: str = pydantic.Field(..., max_length=3, description="Unit of fuel consumption")
    tankcap: float = pydantic.Field(..., description="Maximum fuel capacity")
    cap_unit: str = pydantic.Field(..., max_length=3, description="Mass unit")
    weight: float = pydantic.Field(..., description="Maximum weight")
    wei_unit: str = pydantic.Field(..., max_length=3, description="Mass unit of weight")
    span: float = pydantic.Field(..., description="Span")
    span_unit: str = pydantic.Field(..., max_length=3, description="Span unit")
    leng: float = pydantic.Field(..., description="Length of plane")
    leng_unit: str = pydantic.Field(..., max_length=3, description="Length unit")
    op_speed: float = pydantic.Field(..., description="Operating speed")
    speed_unit: str = pydantic.Field(..., max_length=3, description="Speed unit")
    producer: str = pydantic.Field(..., max_length=5, description="Plane manufacturer")
    seatsmax_b: int = pydantic.Field(..., description="Maximum capacity in business class")
    seatsmax_f: int = pydantic.Field(..., description="Maximum capacity in first class")


class FlightScheduleValidator(pydantic.BaseModel):
    carrid: str = pydantic.Field(..., max_length=3, description="Airline code")
    connid: str = pydantic.Field(..., max_length=4, description="Connection ID")
    countryfr: str = pydantic.Field(..., max_length=3, description="From country")
    cityfrom: str = pydantic.Field(..., max_length=20, description="From city")
    airpfrom: str = pydantic.Field(..., description="From airport")
    countryto: str = pydantic.Field(..., max_length=3, description="To country")
    cityto: str = pydantic.Field(..., max_length=20, description="To city")
    airpto: str = pydantic.Field(..., description="To airport")
    fltime: int = pydantic.Field(..., description="Flight time")
    deptime: str = pydantic.Field(..., description="Departure time")
    arrtime: str = pydantic.Field(..., description="Arrival time")
    distance: float = pydantic.Field(..., description="Distance")
    distid: str = pydantic.Field(..., max_length=3, description="Distance ID")
    fltype: str = pydantic.Field(..., max_length=3, description="Flight type")
    period: str = pydantic.Field(..., max_length=3, description="Period")


class FlightValidator(pydantic.BaseModel):
    carrid: str = pydantic.Field(..., max_length=3, description="Airline code")
    connid: str = pydantic.Field(..., max_length=4, description="Connection ID")
    fldate: str = pydantic.Field(..., description="Flight date")
    price: float = pydantic.Field(..., description="Price")
    currency: str = pydantic.Field(..., max_length=5, description="Currency")
    planetype: str = pydantic.Field(..., description="Plane type")
    seatsmax: int = pydantic.Field(..., description="Maximum seats")
    seatsocc: int = pydantic.Field(..., description="Occupied seats")
    paymentsum: float = pydantic.Field(..., description="Payment sum")
    seatsmax_b: int = pydantic.Field(..., description="Maximum seats in business class")
    seatsocc_b: int = pydantic.Field(..., description="Occupied seats in business class")
    seatsmax_f: int = pydantic.Field(..., description="Maximum seats in first class")
    seatsocc_f: int = pydantic.Field(..., description="Occupied seats in first class")


class FlightsValidator(pydantic.BaseModel):
    flights: List[FlightValidator]


class LanguageValidator(pydantic.BaseModel):
    spras: str = pydantic.Field(..., max_length=1, description="Language code")
    laspez: str = pydantic.Field(..., description="Language specifications")
    lahq: str = pydantic.Field(..., description="Degree of translation of language")
    laiso: str = pydantic.Field(..., max_length=2, description="SAP language code")


class CustomerValidator(pydantic.BaseModel):
    id: int = pydantic.Field(..., description="Customer ID")
    name: str = pydantic.Field(..., max_length=25, description="Customer name")
    form: str = pydantic.Field(..., max_length=15, description="Customer form")
    street: str = pydantic.Field(..., max_length=30, description="Customer street")
    postbox: str = pydantic.Field(..., max_length=10, description="Customer postbox")
    postcode: str = pydantic.Field(..., max_length=10, description="Customer postcode")
    city: str = pydantic.Field(..., max_length=25, description="Customer city")
    country: str = pydantic.Field(..., max_length=3, description="Customer country")
    region: str = pydantic.Field(..., max_length=3, description="Customer region")
    telephone: str = pydantic.Field(..., max_length=30, description="Customer telephone")
    custtype: str = pydantic.Field(..., max_length=1, description="Customer type")
    discount: str = pydantic.Field(..., max_length=3, description="Customer discount")
    langu: str = pydantic.Field(..., description="Customer language")
    email: str = pydantic.Field(..., max_length=40, description="Customer email")
    webuser: str = pydantic.Field(..., max_length=25, description="Customer web user")


class UnitValidator(pydantic.BaseModel):
    msehi: str = pydantic.Field(..., max_length=3, description="Unit code")


class CounterValidator(pydantic.BaseModel):
    carrid: str = pydantic.Field(..., max_length=3, description="Airline code")
    countnum: int = pydantic.Field(..., description="Counter number")
    airport: str = pydantic.Field(..., description="Airport code")


class AgencyValidator(pydantic.BaseModel):
    agencynum: int = pydantic.Field(..., description="Agency number")
    name: str = pydantic.Field(..., max_length=25, description="Agency name")
    street: str = pydantic.Field(..., max_length=30, description="Agency street")
    postbox: str = pydantic.Field(..., max_length=10, description="Agency postbox")
    postcode: str = pydantic.Field(..., max_length=10, description="Agency postcode")
    city: str = pydantic.Field(..., max_length=25, description="Agency city")
    country: str = pydantic.Field(..., max_length=3, description="Agency country")
    region: str = pydantic.Field(..., max_length=3, description="Agency region")
    telephone: str = pydantic.Field(..., max_length=30, description="Agency telephone")
    url: str = pydantic.Field(..., max_length=255, description="Agency URL")
    langu: str = pydantic.Field(..., description="Agency language")
    currency: str = pydantic.Field(..., max_length=5, description="Agency currency")


class BookingValidator(pydantic.BaseModel):
    carrid: str = pydantic.Field(..., max_length=3, description="Airline code")
    connid: str = pydantic.Field(..., max_length=4, description="Connection ID")
    fldate: str = pydantic.Field(..., description="Flight date")
    bookid: int = pydantic.Field(..., description="Booking ID")  # TODO: numeric
    customid: int = pydantic.Field(..., description="Customer ID")  # TODO: numeric
    custtype: str = pydantic.Field(..., max_length=1, description="Customer type")  # TODO: add validation
    smoker: str = pydantic.Field(..., max_length=1, description="Smoker flag")  # TODO: add boolean field
    luggweight: int = pydantic.Field(..., ge=0, le=99999999, description="Luggage weight")
    wunit: str = pydantic.Field(..., description="Weight unit")
    invoice: str = pydantic.Field(..., description="Invoice flag")
    class_: str = pydantic.Field(..., max_length=1, description="Class")
    forcuram: float = pydantic.Field(..., description="Foreign currency amount")
    forcurkey: str = pydantic.Field(..., description="Foreign currency key")
    loccuram: float = pydantic.Field(..., description="Local currency amount")
    loccurkey: str = pydantic.Field(..., description="Local currency key")
    order_date: str = pydantic.Field(..., description="Order date")
    # optional
    counter: Optional[int] = pydantic.Field(..., description="Counter number")
    agencynum: Optional[int] = pydantic.Field(..., description="Agency number")
    cancelled: Optional[str] = pydantic.Field(..., description="Cancelled flag")
    reserved: Optional[str] = pydantic.Field(..., description="Reserved flag")
    passname: Optional[str] = pydantic.Field(..., max_length=25, description="Passenger name")
    passform: Optional[str] = pydantic.Field(..., max_length=15, description="Passenger form")
    passbirth: Optional[str] = pydantic.Field(..., description="Passenger birth date")


class BookingsValidator(pydantic.BaseModel):
    bookings: List[BookingValidator]


# ============ Resources ============


class FlightResource:
    @api.validate(resp=Response(HTTP_200=FlightValidator), tags=[flight_tag])
    def on_get_single(self, req, resp, airline_code, connection_id, flight_date):
        """Retrieve a single flight by airline code, connection ID, and flight date.

        This endpoint retrieves detailed information about a specific flight based on the provided airline code,
        connection ID, and flight date.
        """
        try:
            flight = FlightModel.select().where(
                FlightModel.carrid == airline_code,
                FlightModel.connid == connection_id,
                FlightModel.fldate == flight_date
            ).dicts().get()
        except peewee.DoesNotExist:
            raise falcon.HTTPNotFound
        else:
            resp.media = flight
            resp.status = falcon.HTTP_200

    @api.validate(resp=Response(HTTP_200=FlightsValidator), tags=[flight_tag])
    def on_get_all(self, req, resp):
        """Retrieve a list of all flights.

        This endpoint retrieves a list of all flights available.
        """
        # Pagination in PeeWee is supported
        # Ref: https://docs.peewee-orm.com/en/latest/peewee/querying.html#paginating-records
        flights = FlightModel.select().dicts()
        resp.media = {"flights": [flight for flight in flights]}
        resp.status = falcon.HTTP_200


class AirportResource:
    @api.validate(resp=Response(HTTP_200=AirportsValidator), tags=[airport_tag])
    def on_get_all(self, req, resp):
        """Retrieve a list of all airports.

        This endpoint retrieves a list of all airports available.
        """
        airports = AirportModel.select().dicts()
        resp.media = {"airports": [airport for airport in airports]}
        resp.status = falcon.HTTP_200


class CityResource:
    @api.validate(resp=Response(HTTP_200=CitiesValidator), tags=[city_tag])
    def on_get_all(self, req, resp):
        """Retrieve a list of all cities.

        This endpoint retrieves a list of all cities available.
        """
        cities = CityModel.select().dicts()
        resp.media = {"cities": [city for city in cities]}
        resp.status = falcon.HTTP_200


class CountryResource:
    @api.validate(resp=Response(HTTP_200=CountriesValidator), tags=[country_tag])
    def on_get_all(self, req, resp):
        """Retrieve a list of all countries.

        This endpoint retrieves a list of all countries available.
        """
        # Ref: https://docs.peewee-orm.com/en/latest/peewee/api.html?highlight=distinct#Select.distinct
        countries = CityModel.select(CityModel.country).distinct().dicts()
        resp.media = {"countries": [country for country in countries]}
        resp.status = falcon.HTTP_200


class BookingResource:
    @api.validate(resp=Response(HTTP_200=BookingValidator), tags=[booking_tag])
    def on_get_single(self, req, resp, airline_code, connection_id, flight_date, booking_id):
        """Retrieve a single booking by airline code, connection ID, flight date, and booking ID.

        This endpoint retrieves detailed information about a specific booking based on the provided airline code,
        connection ID, flight date, and booking ID.
        """
        try:
            booking = BookingModel.select().where(
                BookingModel.carrid == airline_code,
                BookingModel.connid == connection_id,
                BookingModel.fldate == flight_date,
                BookingModel.bookid == booking_id
            ).dicts().get()
        except peewee.DoesNotExist:
            raise falcon.HTTPNotFound
        else:
            resp.media = booking
            resp.status = falcon.HTTP_200

    @api.validate(json=BookingValidator, tags=[booking_tag])
    def on_post(self, req, resp):
        """Create a new booking.

        This endpoint creates a new booking based on the provided data in the request body.
        """

    @api.validate(json=BookingValidator, tags=[booking_tag])
    def on_put(self, req, resp):
        """Update an existing booking.

        This endpoint updates an existing booking based on the provided data in the request body.
        """

    @api.validate(resp=Response(HTTP_200=BookingsValidator), tags=[booking_tag])
    def on_get_all(self, req, resp):
        """Retrieve a list of all bookings.

        This endpoint retrieves a list of all bookings available.
        """
        bookings = BookingModel.select().dicts()
        resp.media = {"bookings": [booking for booking in bookings]}
        resp.status = falcon.HTTP_200


# ============ Services ============


class DatabaseManager:
    _models = [
        AirlineModel, TimezoneRuleModel, SummerTimeRuleModel, TimezoneModel, AirportModel, CityModel,
        FlightScheduleModel, CurrencyModel, PlaneModel, FlightModel, LanguageModel, CustomerModel,
        UnitModel, CounterModel, AgencyModel, BookingModel
    ]

    def __init__(self, name: str, sql_database: peewee.SqliteDatabase = database):
        self._name = name
        self._database = sql_database

    def init_and_create_tables(self):
        self._database.init(self._name)
        self._database.create_tables(self._models)

    def get_database(self):
        return self._database

    def create_tables(self):
        self._database.create_tables(self._models)

    def drop_tables(self):
        self._database.drop_tables(self._models)


class AppService(falcon.App):
    prefix = "/api/v1"  # TODO: move to settings?

    def __init__(self, database_manager: DatabaseManager):
        super().__init__()

        self._database_manager = database_manager
        self._database_manager.init_and_create_tables()

        flight = FlightResource()
        airport = AirportResource()
        city = CityResource()
        country = CountryResource()
        booking = BookingResource()

        self.add_route(self.prefix + "/flight/{airline_code}/{connection_id}/{flight_date}", flight, suffix="single")
        self.add_route(self.prefix + "/flight", flight)
        self.add_route(self.prefix + "/flights", flight, suffix="all")
        self.add_route(self.prefix + "/airports", airport, suffix="all")
        self.add_route(self.prefix + "/cities", city, suffix="all")
        self.add_route(self.prefix + "/countries", country, suffix="all")
        self.add_route(self.prefix + "/booking/{airline_code}/{connection_id}/{flight_date}/{booking_id}", booking, suffix="single")
        self.add_route(self.prefix + "/booking", booking)
        self.add_route(self.prefix + "/bookings", booking, suffix="all")

        api.register(self)

    def get_database_manager(self):
        return self._database_manager
