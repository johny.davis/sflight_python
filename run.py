from settings import db_name, server_host, server_port, server_debug
from wsgiref.simple_server import make_server
from main import DatabaseManager, AppService

if __name__ == '__main__':
    database_manager = DatabaseManager(db_name)
    app = AppService(database_manager)

    with make_server(server_host, server_port, app) as httpd:
        print(f"Server running on http://{server_host}:{server_port} (Debug mode: {server_debug})")
        print(f"Swagger running on http://{server_host}:{server_port}/apidoc/swagger")
        httpd.serve_forever()
