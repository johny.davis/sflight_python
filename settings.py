import pathlib
import configparser
import spectree

api = spectree.SpecTree("falcon")

root = pathlib.Path(__file__).parent

config = configparser.ConfigParser()
config.read(root / 'settings.ini')

# Database settings
db_host = config.get('Database', 'host')
db_port = config.getint('Database', 'port')
db_name = config.get('Database', 'name')
db_user = config.get('Database', 'user')
db_password = config.get('Database', 'password')

# Server settings
server_host = config.get('Server', 'host')
server_port = config.getint('Server', 'port')
server_debug = config.getboolean('Server', 'debug')

# JWT settings
jwt_secret_key = config.get('JWT', 'secret_key')
jwt_algorithm = config.get('JWT', 'algorithm')
jwt_access_token_expiration = config.getint('JWT', 'access_token_expiration_seconds')
jwt_refresh_token_expiration = config.getint('JWT', 'refresh_token_expiration_seconds')
