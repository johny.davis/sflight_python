import falcon
import pytest
import peewee
from falcon import testing

from main import (DatabaseManager, AirlineModel, TimezoneRuleModel, SummerTimeRuleModel, TimezoneModel, AirportModel,
                  CityModel,
                  FlightScheduleModel, CurrencyModel, PlaneModel, FlightModel, AppService, LanguageModel, CustomerModel,
                  UnitModel, CounterModel, AgencyModel, BookingModel)


@pytest.fixture
def app() -> AppService:
    database_manager = DatabaseManager(":memory:")
    return AppService(database_manager)


@pytest.fixture
def client(app):
    return testing.TestClient(app)


@pytest.fixture
def db(app) -> peewee.SqliteDatabase:
    database_manager = app.get_database_manager()
    with database_manager.get_database():
        database_manager.create_tables()
        yield database_manager.get_database()
        database_manager.drop_tables()


@pytest.fixture
def create_flight(db):
    with db:
        # no dependencies
        airline_curr = CurrencyModel.create(
            currkey="USD",
            currdec=2
        )
        # awaits `CurrencyModel`
        airline = AirlineModel.create(
            carrid="AA",  # primary key
            carrname="American Airlines",
            currcode=airline_curr,  # foreign key
            url="http://www.aa.com"
        )
        # no dependencies
        tz_rule_from = TimezoneRuleModel.create(
            zonerule="M0500",
            utcdiff="050000",
            utcsign="-",
            flagactive="X"
        )
        # no dependencies
        st_rule_from = SummerTimeRuleModel.create(
            dstrule="NONE",
            dstdiff="000000",
            flagactive="X"
        )
        # awaits `TimezoneRuleModel` and `SummerTimeRuleModel`
        timezone_from = TimezoneModel.create(
            tzone="UTC-5",  # primary key
            zonerule=tz_rule_from,  # foreign key
            dstrule=st_rule_from,  # foreign key
            flagactive="X"
        )
        # awaits `TimezoneModel`
        airport_from = AirportModel.create(
            id="JFK",  # primary key
            name="New York JF Kennedy, USA",
            time_zone=timezone_from  # foreign key
        )
        # no dependencies
        tz_rule_to = TimezoneRuleModel.create(
            zonerule="M0800",
            utcdiff="080000",
            utcsign="-",
            flagactive="X"
        )
        # no dependencies
        st_rule_to = st_rule_from
        # awaits `TimezoneRuleModel` and `SummerTimeRuleModel`
        timezone_to = TimezoneModel.create(
            tzone="UTC-8",  # primary key
            zonerule=tz_rule_to,  # foreign key
            dstrule=st_rule_to,  # foreign key
            flagactive="X"
        )
        # awaits `TimezoneModel`
        airport_to = AirportModel.create(
            id="SFO",  # primary key
            name="San Francisco Int Apt,USA",
            time_zone=timezone_to  # foreign key
        )
        # no dependencies
        loc_from = CityModel.create(
            city="NEW YORK",
            country="US",
            latitude=4.07,
            longitude=7.4,
        )
        # no dependencies
        loc_to = CityModel.create(
            city="SAN FRANCISCO",
            country="US",
            latitude=3.7766,
            longitude=1.22416,
        )
        # awaits `AirlineModel`, 2 x `AirportModel` and 2 x `CityModel` (composite foreign key)
        schedule = FlightScheduleModel.create(
            carrid=airline,  # part of composite primary key
            connid="0017",  # part of composite primary key
            countryfr=loc_from.country,  # foreign key
            cityfrom=loc_from.city,  # foreign key
            airpfrom=airport_from,  # foreign key
            countryto=loc_to.country,  # foreign key
            cityto=loc_to.city,  # foreign key
            airpto=airport_to,  # foreign key
            # ...
            fltime=361,
            deptime=110000,
            arrtime=140100,
            distance=2572,
            distid="MI",
            fltype="",
            period=0
        )
        # no dependencies
        curr = airline_curr
        # no dependencies
        plane = PlaneModel.create(
            planetype="747-400",  # primary key
            seatsmax=385,
            consum=9.95,
            con_unit="KGH",
            tankcap=12345,
            cap_unit="L",
            weight=177670,
            wei_unit="KG",
            span=6.429,
            span_unit="M",
            leng=7.07,
            leng_unit="M",
            op_speed=490,
            speed_unit="KT",
            producer="BOE",
            seatsmax_b=31,
            seatsmax_f=21
        )
        # awaits `FlightScheduleModel`, `CurrencyModel` and `PlaneModel`
        flight = FlightModel.create(
            carrid=schedule.carrid,  # part of composite primary key
            connid=schedule.connid,  # part of composite primary key
            fldate="20180110",  # part of composite primary key
            # ...
            price=422.94,
            # ...
            currency=curr,  # foreign key
            planetype=plane,  # foreign key
            # ...
            seatsmax=385,
            seatsocc=357,
            paymentsum=193846.31,
            seatsmax_b=31,
            seatsocc_b=31,
            seatsmax_f=21,
            seatsocc_f=19
        )
    return flight


@pytest.fixture
def create_booking(db, create_flight):
    flight = create_flight

    with db:
        # no dependencies
        lang = LanguageModel.create(spras="E", laspez="S", lahq="1", laiso="EN")

        # awaits ´LanguageModel´
        customer = CustomerModel.create(
            id="1",
            name="Moinsen Bormann",
            form="Herr",
            street="Caspar-David-Friedrich-Str. 13",
            postbox="",
            postcode="69190",
            city="Walldorf",
            country="DE",
            region="",
            telephone="0123 456789",
            custtype="P",
            discount="15",
            langu=lang,
            email="moinsen.bormann@moin.de",
            webuser=""
        )

        # no dependencies
        unit = UnitModel.create(msehi="kg")

        # no dependencies
        _curr_uah = CurrencyModel.create(currkey="UAH", currdec=2)
        curr_foreign = _curr_uah
        curr_local = _curr_uah

        # awaits `AirportModel`
        # counter = CounterModel()

        # awaits ´LanguageModel´
        # agency = AgencyModel()

        # awaits `FlightModel`, `CustomerModel`, `UnitModel`, 2 x `CurrencyModel`
        # optional `CounterModel` and `AgencyModel`
        booking = BookingModel.create(
            carrid=flight.carrid,
            connid=flight.carrid,
            fldate=flight.fldate,
            bookid=1,
            customid=customer,  # foreign key
            custtype="P",
            smoker="X",
            luggweight=17.4000,
            wunit=unit,  # foreign key
            invoice=" ",
            class_="C",
            forcuram=875.90,
            forcurkey=curr_foreign,  # foreign key
            loccuram=875.90,
            loccurkey=curr_local,  # foreign key
            order_date="20171028"
        )

    return booking


# Tests


def test_get_flight_single(client, create_flight):
    flight = create_flight
    airline_code = flight.carrid
    connection_id = flight.connid
    flight_date = flight.fldate
    response = client.simulate_get(f'/api/v1/flight/{airline_code}/{connection_id}/{flight_date}')

    # general checks
    assert response.status == falcon.HTTP_200
    assert response.json, "response json must have flight data"
    assert len(response.json) == 13, "response json must have 13 fields"
    # check each field
    assert response.json['carrid'] == 'AA'
    assert response.json['connid'] == '0017'
    assert response.json['fldate'] == '20180110'
    assert response.json['price'] == 422.94
    assert response.json['currency'] == 'USD'
    assert response.json['planetype'] == '747-400'
    assert response.json['seatsmax'] == 385
    assert response.json['seatsocc'] == 357
    assert response.json['paymentsum'] == 193846.31
    assert response.json['seatsmax_b'] == 31
    assert response.json['seatsocc_b'] == 31
    assert response.json['seatsmax_f'] == 21
    assert response.json['seatsocc_f'] == 19


def test_get_flights_all(client, create_flight):
    _ = create_flight
    response = client.simulate_get(f'/api/v1/flights')
    flights = response.json.get("flights")

    assert flights is not None, "no flights in json"
    assert len(flights) == 1, "json must have one flight"
    assert response.status == falcon.HTTP_200


def test_get_airports_all(client, create_flight):
    _ = create_flight
    response = client.simulate_get(f'/api/v1/airports')
    airports = response.json.get("airports")

    assert airports is not None, "no airports in json"
    assert len(airports) == 2, "json must have two airports"
    assert response.status == falcon.HTTP_200


def test_get_cities_all(client, create_flight):
    _ = create_flight
    response = client.simulate_get(f'/api/v1/cities')
    cities = response.json.get("cities")

    assert cities is not None, "no cities in json"
    assert len(cities) == 2, "json must have two cities"
    assert response.status == falcon.HTTP_200


def test_get_countries_all(client, create_flight):
    _ = create_flight
    response = client.simulate_get(f'/api/v1/countries')
    countries = response.json.get("countries")

    assert countries is not None, "no countries in json"
    assert len(countries) == 1, "json must have one country"
    assert response.status == falcon.HTTP_200


def test_get_bookings_all(client, create_booking):
    _ = create_booking
    response = client.simulate_get(f'/api/v1/bookings')
    bookings = response.json.get("bookings")

    assert bookings is not None, "no bookings in json"
    assert len(bookings) == 1, "json must have one booking"
    assert response.status == falcon.HTTP_200


def test_get_booking_single(client, create_booking):
    booking = create_booking
    airline_code = booking.carrid
    connection_id = booking.connid
    flight_date = booking.fldate
    booking_id = booking.bookid
    response = client.simulate_get(f'/api/v1/booking/{airline_code}/{connection_id}/{flight_date}/{booking_id}')

    assert response.status == falcon.HTTP_200


def test_get_flights_all_with_empty_db(client, db):
    response = client.simulate_get('/api/v1/flights')
    flights = response.json.get("flights")

    assert flights is not None, "No flights in JSON"
    assert len(flights) == 0, "JSON should be empty"
    assert response.status == falcon.HTTP_200


def test_get_airports_all_with_empty_db(client, db):
    response = client.simulate_get('/api/v1/airports')
    airports = response.json.get("airports")

    assert airports is not None, "No airports in JSON"
    assert len(airports) == 0, "JSON should be empty"
    assert response.status == falcon.HTTP_200


def test_get_cities_all_with_empty_db(client, db):
    response = client.simulate_get('/api/v1/cities')
    cities = response.json.get("cities")

    assert cities is not None, "No cities in JSON"
    assert len(cities) == 0, "JSON should be empty"
    assert response.status == falcon.HTTP_200


def test_get_countries_all_with_empty_db(client, db):
    response = client.simulate_get('/api/v1/countries')
    countries = response.json.get("countries")

    assert countries is not None, "No countries in JSON"
    assert len(countries) == 0, "JSON should be empty"
    assert response.status == falcon.HTTP_200


def test_get_bookings_all_with_empty_db(client, db):
    response = client.simulate_get('/api/v1/bookings')
    bookings = response.json.get("bookings")

    assert bookings is not None, "No bookings in JSON"
    assert len(bookings) == 0, "JSON should be empty"
    assert response.status == falcon.HTTP_200


def test_get_booking_single_with_empty_db(client, db):
    airline_code = "AA"
    connection_id = "0017"
    flight_date = "20180110"
    booking_id = 1
    response = client.simulate_get(f'/api/v1/booking/{airline_code}/{connection_id}/{flight_date}/{booking_id}')

    assert response.status == falcon.HTTP_404
