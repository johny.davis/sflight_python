import pytest


class MoinError(Exception):
    """There is two kinds of errors: syntax errors and exceptions.

    All exceptions must be instances of a class that derives from BaseException.
    Programmers are encouraged to derive new exceptions from the `Exception` class.
    `Exception` is the base class of all the non-fatal exceptions.
    Examples of fatal exceptions: `SystemExit` and `KeyboardInterrupt`.
    It is good practice to be as specific as possible with the types of exceptions that we intend to handle.
    The use of else is better than adding additional cote to the try: it avoids accidentally catching an exception.
    NEW: raise allows an optional `from` clause:
    > raise RuntimeError from exc
    > raise RuntimeError from None
    If there is more than one exception at time, use `ExceptionGroup`.
    """
    pass


@pytest.fixture(autouse=True)
def some_autouse_fixture():
    # Autouse fixtures are executed first within their scope
    print("Haha autoMOIN!")


def raise_moin():
    raise MoinError


@pytest.mark.skip("Haha, MOIN! Skip moin!")
def test_skip_moin():
    assert "Moin"


def test_raise_moin():
    with pytest.raises(MoinError):
        raise_moin()


def test_with_fixture(tmp_path):
    print(tmp_path)
    assert True


class TestMoinClass:
    def test_moin_a(self):
        ...

    def test_moin_b(self):
        ...
